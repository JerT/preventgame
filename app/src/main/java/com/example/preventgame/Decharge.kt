package com.example.preventgame


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import model.Jauge

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */

class Decharge : Fragment() {
    val faim = Jauge(1, "faim", 120, 80)
    val energie = Jauge(1, "energie", 100, 100)

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?

    ): View? {
        val v = inflater.inflate(R.layout.decharge, container, false)

        val bundle = this.arguments
        var nameReceive =  bundle?.getString("keyName")
        if (nameReceive != null) {
            v.findViewById<TextView>(R.id.txt_nom_joueur).setText(nameReceive);
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        val bundle = Bundle()
        bundle.putString("keyName", view.findViewById<TextView>(R.id.txt_nom_joueur).text.toString())

        view.findViewById<Button>(R.id.btn_carte).setOnClickListener {
            findNavController().navigate(R.id.action_decharge_to_carte, bundle)
        }
        view.findViewById<Button>(R.id.button_back).setOnClickListener {
            findNavController().navigate(R.id.action_decharge_to_ChapSelect, bundle)
        }

        view.findViewById<Button>(R.id.btn_chercher).setOnClickListener {
            var valfa = view.findViewById<TextView>(R.id.val_faim).text.toString()
            var vale = view.findViewById<TextView>(R.id.val_nrj).text.toString()
            view.findViewById<TextView>(R.id.val_nrj).setText((Integer.parseInt(vale)-40).toString());
            view.findViewById<TextView>(R.id.val_faim).setText((Integer.parseInt(valfa)-15).toString());
            var randnum = (0..10).random()
            view.findViewById<TextView>(R.id.txtConv).setText("Tu as trouvé l'objet "+randnum.toString())
        }
        view.findViewById<Button>(R.id.btn_action1).setOnClickListener {
            var val1 = view.findViewById<TextView>(R.id.val_nrj).text.toString()

            view.findViewById<TextView>(R.id.val_nrj).setText(energie.valeurMax.toString());
        }

        view.findViewById<Button>(R.id.btn_action2).setOnClickListener {
            view.findViewById<TextView>(R.id.val_faim).setText(faim.valeurMax.toString());
        }



        view.findViewById<Button>(R.id.button_reset).setOnClickListener {
            view.findViewById<TextView>(R.id.val_faim).setText(faim.valeurMax.toString());
            view.findViewById<TextView>(R.id.val_nrj).setText(energie.valeurMax.toString());
        }
    }
}