package com.example.preventgame


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import model.Jauge

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */

class Ecole : Fragment() {
    val faim = Jauge(1, "faim", 120, 80)
    val etude = Jauge(1, "fan", 100000, 0)
    val energie = Jauge(1, "energie", 100, 100)

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?

    ): View? {
        val v = inflater.inflate(R.layout.ecole, container, false)

        val bundle = this.arguments
        var nameReceive =  bundle?.getString("keyName")
        if (nameReceive != null) {
            v.findViewById<TextView>(R.id.txt_nom_joueur).setText(nameReceive);
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        val bundle = Bundle()
        bundle.putString("keyName", view.findViewById<TextView>(R.id.txt_nom_joueur).text.toString())

        view.findViewById<Button>(R.id.btn_carte).setOnClickListener {
            findNavController().navigate(R.id.action_ecole_to_carte, bundle)
        }
        view.findViewById<Button>(R.id.button_back).setOnClickListener {
            findNavController().navigate(R.id.action_ecole_to_ChapSelect, bundle)
        }

        view.findViewById<Button>(R.id.btn_action1).setOnClickListener {
            var val1 = view.findViewById<TextView>(R.id.val_faim).text.toString()
            var val2 = view.findViewById<TextView>(R.id.val_nrj).text.toString()
            var val3 = view.findViewById<TextView>(R.id.val_int).text.toString()
            view.findViewById<TextView>(R.id.val_faim).setText((Integer.parseInt(val1)-20).toString());
            view.findViewById<TextView>(R.id.val_nrj).setText((Integer.parseInt(val2)-15).toString());
            view.findViewById<TextView>(R.id.val_int).setText((Integer.parseInt(val3)+15).toString());
        }

        view.findViewById<Button>(R.id.btn_action2).setOnClickListener {
            view.findViewById<TextView>(R.id.val_nrj).setText(energie.valeurMax.toString());
        }



        view.findViewById<Button>(R.id.button_reset).setOnClickListener {
            view.findViewById<TextView>(R.id.val_faim).setText(faim.valeurMax.toString());
            view.findViewById<TextView>(R.id.val_int).setText(0.toString());
            view.findViewById<TextView>(R.id.val_nrj).setText(energie.valeurMax.toString());
        }
    }
}