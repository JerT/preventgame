package com.example.preventgame


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import model.Jauge

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */

class SecondFragment : Fragment() {
    val faim = Jauge(1, "faim", 120, 80)
    val fan = Jauge(1, "fan", 100000, 0)
    val energie = Jauge(1, "energie", 100, 100)

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?

    ): View? {
        val v = inflater.inflate(R.layout.fragment_second, container, false)

        val bundle = this.arguments
        var nameReceive =  bundle?.getString("keyName")
        if (nameReceive != null) {
            v.findViewById<TextView>(R.id.txt_nom_joueur).setText(nameReceive);
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        /*
        val intent = Intent(view.context, SecondFragment::class.java)
        if (null != intent.getStringExtra("Username")) {
            val profileName = intent.getStringExtra("Username")
            view.findViewById<TextView>(R.id.txt_nom_joueur).setText(profileName);
        }
        */
        val bundle = Bundle()
        bundle.putString("keyName", view.findViewById<TextView>(R.id.txt_nom_joueur).text.toString())

        view.findViewById<Button>(R.id.btn_carte).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_carte, bundle)
        }
        view.findViewById<Button>(R.id.button_back).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_choixChap, bundle)
        }

        view.findViewById<Button>(R.id.btn_action1).setOnClickListener {
            var val1 = view.findViewById<TextView>(R.id.val_faim).text.toString()
            view.findViewById<TextView>(R.id.val_faim).setText((Integer.parseInt(val1)-40).toString());
            view.findViewById<TextView>(R.id.val_nrj).setText("100");
        }

        view.findViewById<Button>(R.id.btn_action2).setOnClickListener {
            view.findViewById<TextView>(R.id.val_faim).setText(faim.valeurMax.toString());
        }


        view.findViewById<Button>(R.id.btn_chercher).setOnClickListener {
            var valfa = view.findViewById<TextView>(R.id.val_faim).text.toString()
            var valf = view.findViewById<TextView>(R.id.val_int).text.toString()
            var vale = view.findViewById<TextView>(R.id.val_nrj).text.toString()
            view.findViewById<TextView>(R.id.val_nrj).setText((Integer.parseInt(vale)-40).toString());
            view.findViewById<TextView>(R.id.val_faim).setText((Integer.parseInt(valfa)-15).toString());
            view.findViewById<TextView>(R.id.val_int).setText((Integer.parseInt(valf)+25).toString());
        }

        view.findViewById<Button>(R.id.button_reset).setOnClickListener {
            view.findViewById<TextView>(R.id.val_faim).setText(faim.valeurMax.toString());
            view.findViewById<TextView>(R.id.val_int).setText(0.toString());
            view.findViewById<TextView>(R.id.val_nrj).setText(energie.valeurMax.toString());
        }
    }
}