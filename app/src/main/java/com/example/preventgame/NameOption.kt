package com.example.preventgame

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.navigation.fragment.findNavController

class NameOption : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bundle = this.arguments
        var view = inflater.inflate(R.layout.name_option, container, false);
        // Inflate the layout for this fragment
        val nameEditText : EditText = view.findViewById<EditText>(R.id.name);
        val sharedPref: SharedPreferences = requireActivity().getSharedPreferences("preventGame", 0);
        if(sharedPref.getString("userString", "") == ""){
            nameEditText.setText("Pierre");
        }else if(bundle?.getBoolean("menu") != null){

        }else{
            findNavController().navigate(R.id.name_option_to_menu)
        }
        view.findViewById<Button>(R.id.validationButton).setOnClickListener {
            val editor = sharedPref.edit()
            editor.putString("userString", view.findViewById<TextView>(R.id.name).text.toString())
            editor.apply()
            findNavController().navigate(R.id.name_option_to_menu)
        }
        return view
    }


}