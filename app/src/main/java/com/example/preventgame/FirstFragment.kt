package com.example.preventgame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import model.Utilisateur
import modelView.UserViewModel


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.options_screen, container, false)
        var model = ViewModelProvider(requireActivity()).get(UserViewModel::class.java);
        if (model.getUser().value == null){
            model.setUtilisateur(Utilisateur());
        }
        val bundle = this.arguments
        // Inflate the layout for this fragment
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.btn_ch3).setOnClickListener {
            /*val intent = Intent(getActivity(), SecondFragment::class.java)
            intent.putExtra("Username",view.findViewById<TextView>(R.id.playerName).text)
            getActivity().startActivity(intent)
            */
            val bundle = Bundle()


            findNavController().navigate(R.id.action_FirstFragment_to_choixChap, bundle)
        }
        view.findViewById<TextView>(R.id.btn_ejeu).setOnClickListener {
            val bundle = Bundle()
            findNavController().navigate(R.id.action_FirstFragment_to_Credits, bundle)
        }
        view.findViewById<TextView>(R.id.btn_feedback).setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_feedback)
        }
        view.findViewById<AppCompatImageButton>(R.id.btn_option_name).setOnClickListener{
            val bundle = Bundle();
            bundle.putBoolean("menu",true)
            findNavController().navigate(R.id.action_FirstFragment_to_NameOption,bundle);
        }

    }
}