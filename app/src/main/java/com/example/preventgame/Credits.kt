package com.example.preventgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.navigation.fragment.findNavController
import model.Texte

/**
 * Ecran des crédits
 */

class Credits : Fragment() {
    var namepl = "name?"
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.credits, container, false)

        val bundle = this.arguments
        var strtext =  bundle?.getString("keyName")
        if (strtext != null) {
            namepl = strtext
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        view.findViewById<AppCompatImageButton>(R.id.btn_maison).setOnClickListener {
            val bundle = Bundle()
            bundle.putString("keyName", namepl)
            findNavController().navigate(R.id.action_credits_to_Main_Menu, bundle)
        }

    }
}