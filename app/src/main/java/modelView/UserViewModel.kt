package modelView

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import model.Utilisateur

class UserViewModel : ViewModel() {
    private var utilisateur = MutableLiveData<Utilisateur>()


    fun setUtilisateur(utilisateur: Utilisateur) {
        this.utilisateur.value = utilisateur
    }

    fun getUser(): LiveData<Utilisateur> {
        return utilisateur
    }


}