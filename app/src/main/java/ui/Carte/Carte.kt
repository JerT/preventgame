package ui.carte

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.preventgame.R

/**
 * Ecran de la carte
 */

class Carte : Fragment() {

    var namepl = "name?"
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.carte, container, false)

        val bundle = this.arguments
        var strtext =  bundle?.getString("keyName")
        if (strtext != null) {
            namepl = strtext
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bundle = Bundle()
        bundle.putString("keyName", namepl)

        view.findViewById<Button>(R.id.btn_maison).setOnClickListener {
            findNavController().navigate(R.id.action_carte_to_choixChap, bundle)
        }
        view.findViewById<Button>(R.id.btn_hopital).setOnClickListener {
            findNavController().navigate(R.id.action_carte_to_choixChap, bundle)
        }
        view.findViewById<Button>(R.id.btn_ecole).setOnClickListener {
            findNavController().navigate(R.id.action_carte_to_choixChap, bundle)
        }

    }
}