package ui.histoire

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.preventgame.R


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ChoixChap : Fragment() {

    var namepl = "name?"

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.chapselect, container, false)

        val bundle = this.arguments
        var nameReceive =  bundle?.getString("keyName")
        if (nameReceive != null) {
            namepl = nameReceive
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bundle = Bundle()
        bundle.putString("keyName", namepl)

        view.findViewById<AppCompatImageButton>(R.id.btn_ejeu).setOnClickListener {

            /*val intent = Intent(getActivity(), SecondFragment::class.java)
            intent.putExtra("Username",view.findViewById<TextView>(R.id.playerName).text)
            getActivity().startActivity(intent)
            */
            findNavController().navigate(R.id.action_ChoixChap_to_Carte, bundle)
        }

        view.findViewById<TextView>(R.id.btn_ch1).setOnClickListener {
            findNavController().navigate(R.id.action_ChoixChap_to_Conv, bundle)
        }


        view.findViewById<AppCompatImageButton>(R.id.btn_maison).setOnClickListener {
            findNavController().navigate(R.id.action_ChoixChap_to_FirstFragment, bundle)

        }
    }
}