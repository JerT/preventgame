package ui.histoire


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import asyncTask.DownloadFilesTask
import com.example.preventgame.R
import com.google.android.material.progressindicator.CircularProgressIndicator
import model.*


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */

class Conv : Fragment(), DownloadFilesTask.DownloadFilesTaskListerner  {
    private var compt :Int = 1;
    private var currentEvent : EventHistoire? = null;
    private var chapitre : Chapitre? = null
    var namepl = "name?"

    private lateinit var txtConv : TextView;
    private lateinit var choix1 : TextView;
    private lateinit var choix2 : TextView;
    private lateinit var personnageImage : ImageView;
    private lateinit var nextBtn : AppCompatImageButton;
    private lateinit var personnageName : TextView;
    private lateinit var circularProgress : CircularProgressIndicator

    private lateinit var linkID : HashMap<String,Int>;

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?

    ): View? {
        val v = inflater.inflate(R.layout.conv, container, false)

        val bundle = this.arguments
        var nameReceive =  bundle?.getString("keyName")
        if (nameReceive != null) {
            namepl = nameReceive
            val sharedPref = activity?.getSharedPreferences("preventGame", 0);
            if (sharedPref != null) {
                namepl = sharedPref.getString("userString", "mc")!!
            };
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        Log.i("conv","creation")
        val bundle = Bundle()
        bundle.putString("keyName", namepl)
        this.txtConv = view.findViewById<TextView>(R.id.txtConv);
        this.choix1 = view.findViewById<TextView>(R.id.btn_choice1);
        this.choix2 = view.findViewById<TextView>(R.id.btn_choice2);
        this.nextBtn = view.findViewById<AppCompatImageButton>(R.id.btn_next);
        this.personnageImage = view.findViewById<ImageView>(R.id.imageView3);
        this.personnageName = view.findViewById<TextView>(R.id.personnageName);
        this.circularProgress = view.findViewById<CircularProgressIndicator>(R.id.circular_progress);

        linkID = HashMap();
        linkID["R.drawable.mc"] = R.drawable.mc
        linkID["R.drawable.equipe"] = R.drawable.equipe
        linkID["R.drawable.porti"] = R.drawable.porti
        linkID["R.drawable.prof"] = R.drawable.prof
        linkID["R.drawable.steven"] = R.drawable.steven
        linkID["R.drawable.laurent"] = R.drawable.laurent
        linkID["R.drawable.mc_voice"] = R.drawable.mc_voice

        view.findViewById<TextView>(R.id.txtConv).visibility = View.GONE
        view.findViewById<TextView>(R.id.btn_choice1).visibility = View.GONE
        view.findViewById<TextView>(R.id.btn_choice2).visibility = View.GONE
        view.findViewById<AppCompatImageButton>(R.id.btn_next).visibility = View.GONE
        view.findViewById<ImageView>(R.id.imageView3).visibility = View.GONE
        this.personnageImage.visibility = View.GONE
        initChapitre(1);
        //defChapitre(0)


        view.findViewById<AppCompatImageButton>(R.id.btn_back).setOnClickListener {
            findNavController().navigate(R.id.action_Conv_to_ChapSelect, bundle)
        }
    }

    private fun resetButton() {
        view?.findViewById<TextView>(R.id.txtConv)?.visibility = View.GONE
        view?.findViewById<TextView>(R.id.btn_choice1)?.visibility = View.GONE
        view?.findViewById<TextView>(R.id.btn_choice2)?.visibility = View.GONE
        view?.findViewById<AppCompatImageButton>(R.id.btn_next)?.visibility = View.GONE
        view?.findViewById<ImageView>(R.id.imageView3)?.visibility = View.GONE
        personnageName.visibility = View.GONE
        compt = 1;
    }

    private fun promptButton(choixevent: Int) {
        var nextEvent: EventHistoire? = null;
        if (choixevent > -1) {
            nextEvent = getEventById(choixevent)
        } else {
            findNavController().navigate(R.id.action_Conv_to_ChapSelect)
        }
        currentEvent = nextEvent
        if (nextEvent is Choix){
            val choice: Choix = nextEvent
            view?.findViewById<TextView>(R.id.btn_choice1)?.visibility = View.VISIBLE
            view?.findViewById<TextView>(R.id.btn_choice2)?.visibility = View.VISIBLE
            view?.findViewById<TextView>(R.id.txtConv)?.visibility = View.VISIBLE
            view?.findViewById<TextView>(R.id.txtConv)?.text = choice.question
            view?.findViewById<TextView>(R.id.btn_choice1)?.text = choice.choices.get(0)
            view?.findViewById<TextView>(R.id.btn_choice2)?.text = choice.choices[1]
            personnageName.visibility = View.VISIBLE
        }
        else if(nextEvent is Conversation) {
            val conversation: Conversation = nextEvent
            personnageName.visibility = View.VISIBLE
            view?.findViewById<TextView>(R.id.txtConv)?.visibility = View.VISIBLE
            view?.findViewById<AppCompatImageButton>(R.id.btn_next)?.visibility = View.VISIBLE
            view?.findViewById<ImageView>(R.id.imageView3)?.visibility  = View.VISIBLE
            personnageImage.setImageResource(conversation.listDialog[0].personnage!!.linkImage)
            if(conversation.listDialog[0].personnage!!.isNpc) {
                personnageName.text = conversation.listDialog[0].personnage!!.name
            }else {
                personnageName.text = namepl
            }
            txtConv.text = conversation.listDialog[0].text
        }
    }

    private fun initChapitre(idChapitre: Int){
        circularProgress.visibility = View.VISIBLE;
        DownloadFilesTask(this, idChapitre).execute();
    }
    private fun defChapitre(idChapitre: Int) {
        var steven: Personnage = Personnage("Steven",R.drawable.steven,true)
        var laurent: Personnage = Personnage("Laurent",R.drawable.laurent,true)
        var MC: Personnage = Personnage("none",R.drawable.mc,false)
        var porti: Personnage = Personnage("porti",R.drawable.porti,true)
        var prof: Personnage = Personnage("prof", R.drawable.prof,true)
        val event0 = Conversation(listOf(1),0, listOf(Dialogue(MC,"C’est la rentrée au lycée XXX, il est 7h30 du matin, il " +
                "faut vite que je me prépare à aller au lycée. J’ai tellement hâte ! J’espère que ça va bien se passer. À vrai dire, " +
                "tout le monde me dit que la vie au lycée est incroyable et qu’il est facile de se faire des amis."),
                Dialogue(MC,"Qu’est-ce que tu en penses ? Porti ?"), Dialogue(porti,"Ah ça c’est on verra bien mais pour le " +
                "moment il faudrait peut-être que tu accélères le pas sinon tu vas finir par arriver en retard !"), Dialogue(MC,
                "Mince !! Il est déjà 7h45 !! Il faut que je me dépêche !"), Dialogue(MC,"Porti est mon ami depuis" +
                " toujours. On se connaît depuis que je suis rentré en primaire. C’est le seul à qui je peux me confier. " +
                "Les autres ont toujours eu l’habitude de m’éviter à cause de ça d’ailleurs.  Ils disaient toujours que je " +
                "parlais tout seul et que Porti n’existait pas. Je trouve ça dommage mais tant pis. Tout ça c’est du passé " +
                "maintenant ! Une nouvelle aventure est sur le point de commencer ! Pas le temps de déprimer !"),Dialogue(MC,
        "Aller ! C’est parti !")))
        val event1 = Choix(listOf(1,2),1,"Aller à l'école? (l'histoire ne commencera pas si tu restes à la maison)", listOf("non","oui"))
        val event2 = Conversation(listOf(3),2, listOf(Dialogue(MC,"Ma nouvelle vie a enfin commencé ! " +
                "Enfin … c’est ce que je croyais. Ça fait déjà deux semaines que les cours ont commencé et toujours " +
                "pas l’ombre d’une conversation normale à l’horizon !"),Dialogue(MC,"Je fais des efforts pourtant " +
                "tu ne trouves pas Porti ?"), Dialogue(porti,"Bon à part le fait que je t’ai entendu dire à Cyrus " +
                "que je ressemblais à un poisson préhistorique pour essayer de le faire rire, je ne vois pas trop …"),
                Dialogue(MC,"Bon ok j’avoue que ce n’était pas le commentaire le plus délicat que j’ai pu te faire mais " +
                "quand même ! "), Dialogue(MC,"Bon, il m’a bien fallut me rendre à l’évidence que le lycée n’allait " +
                "pas régler mon problème par magie. Toujours personne ne voulant accepter de rester avec nous. Il " +
                "fallait que quelque chose se produise."),Dialogue(prof,"Bon il est temps de se mettre au boulot " +
                "bande de tire au flanc ! La rentrée est terminée et j’ai du travail pour vous ! Premièrement, Il va " +
                "falloir que vous mettiez par groupe de 3."), Dialogue(MC,"Et bien la voilà ma chance ! C’est " +
                "l’occasion rêvé ! Je vais enfin pouvoir commencer ma vie palpitante de lycéen !"), Dialogue(MC,"Il " +
                "faudrait juste que tu arrêtes de me présenter n’importe comment et à n’importe et tout se passera mieux " +
                "tu verras !"), Dialogue(MC,"Le travail en question fut de réaliser la vidéo de promotion de la " +
                "classe. Je ne comprends toujours pas l’intérêt mais je savais dès le début que ce prof était … étrange ? " +
                "C’est comme ça que je me suis retrouvé avec Steven et Laurent chez moi pour commencer à réfléchir sur " +
                "le sujet.")))
        val event3 = Choix(listOf(4,5),3,"Travailler chez soi?", listOf("non","oui"))
        val event4 = Conversation(listOf(3),4, listOf(Dialogue(MC,"Mais j'avais vraiment envie de montrer ma " +
                "collection de taille crayon")))
        val event5 = Conversation(listOf(6),5, listOf(Dialogue(MC,"De retour à la maison, il fallait se mettre " +
                "au travail sur la tâche qu’on nous avait confié, à savoir promouvoir la future vidéo de la classe sur " +
                "le net. Mais je n’avais qu’une chose en tête. Au diable cette vidéo ! Il fallait d’abord que j’en apprenne " +
                "plus sur mes camarades. Après tout, pour qu’un projet se déroule bien, il faut une déjà une bonne ambiance. " +
                "Enfin c’est ce que m’a dit Porti …"), Dialogue(steven,"Bon, il serait temps de s’y mettre non ?"),
                Dialogue(MC,"Commençons par Steven. Il y a ses sauts d’humeurs et sa musculature démesurée que me font un " +
                "peu peur parfois. En tout cas, il a l’air d’être quelqu’un d’assez sympathique et sérieux. "), Dialogue(laurent,
                "Du calme ! On est qu’au premier jour ça peut attendre non ?"), Dialogue(steven,"Sauf que ça fait " +
                "déjà 4h qu’on est là à manger des biscuits devant la télé !"), Dialogue(laurent,"Et puis nous avons " +
                "Laurent. Au début j’ai cru qu’il était sourd mais non. Il faut juste beaucoup le secouer pour arriver à " +
                "quelque chose de productif. Très attaché à ses valeurs en tout cas. Le bien-être avant tout ! "),Dialogue(
                porti,"Au pire, tu n’as qu’à essayer de leur proposer une solution qui les arrange tous les deux non ?"),
                Dialogue(MC,"Bon écoutez, ce que je vous propose, c’est que l’on réfléchisse d’abord à quelle plateforme " +
                        "nous allons utiliser pour faire la promotion de notre vidéo. Je pense que c’est déjà bien " +
                        "pour un début non ?"), Dialogue(laurent,"Parfait ! J’aime le travail facile !"), Dialogue(steven,
                "Bon, ce n’est pas comme si j’avais le choix …"), Dialogue(MC,"Nous avons donc décidé d’utiliser " +
                "YouTube pour notre projet. Nous pensons que c’est le meilleur moyen aujourd’hui pour se faire de la publicité " +
                "sur internet. Ce projet à l’air amusant en tout cas."), Dialogue(MC,"Bon voilà. Notre compte est créé !" +
                " Il faudra juste s’occuper de la forme de chaîne plus tard."), Dialogue( steven,"Parfait ! On arrive " +
                "enfin à quelque chose de concret ! C’est pas trop tôt ! "), Dialogue(laurent,"Par contre tu ne nous as " +
                "pas donné le mot de passe. ")))
        val event6  = Choix(listOf(6,0),6,"Donner le mot de passe?", listOf("non","oui"))
        val chapitreTemp: Chapitre = Chapitre("compte piraté",0,"le héros se fait pirater",0, listOf(event0,event1,event2,event3,event4,event5,event6))
        chapitre = chapitreTemp
        currentEvent = chapitreTemp.events.get(0)
    }

    override fun OnDownloadedEvent(chapitre: Chapitre) {
        circularProgress.visibility = View.GONE;
        this.chapitre = chapitre;
        setEventById(chapitre._firstEvent);
    }

    fun setEventById(id : Int){
        promptButton(id)
        nextBtn.setOnClickListener {
            val conversation: Conversation = currentEvent as Conversation

            if (compt < conversation.listDialog.size) {
                personnageImage.setImageResource(conversation.listDialog[compt].personnage!!.linkImage)
                if(conversation.listDialog[compt].personnage!!.isNpc) {
                    personnageName.text = conversation.listDialog[compt].personnage!!.name
                }else {
                    personnageName.text = namepl
                }
                txtConv.text = conversation.listDialog[compt].text
                compt += 1
            }
            else {
                resetButton()
                setEventById(conversation.idNextEvent[0])
            }
        }
        if (currentEvent is Choix){

            choix1.setOnClickListener {

                resetButton()
                setEventById((currentEvent as Choix).idNextEvent[0])

            }
            choix2.setOnClickListener {
                resetButton()
                setEventById((currentEvent as Choix).idNextEvent[1])

            }
        }
    }

    fun getEventById(id : Int) : EventHistoire?{
        chapitre?.events?.forEach{
            if(it.idevent == id){
                return it
            }
        }
        return null;
    }
    override fun getResId(link: String): Int? {
        val resId = -1;
        if(linkID.contains(link)){
            return linkID.get(link)
        }else{
            return R.drawable.porti;
        }
    }
}