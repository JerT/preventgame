package asyncTask

import android.os.AsyncTask
import android.util.Log
import model.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL


class DownloadFilesTask(downloadFilesTaskListener: DownloadFilesTaskListerner, chapitreNum : Int) : AsyncTask<URL?, Int?, JSONObject>() {

    interface DownloadFilesTaskListerner{
        fun OnDownloadedEvent(chapitre: Chapitre);
        fun getResId(link : String) : Int?
    }


    private var downloadFilesTaskListener = downloadFilesTaskListener;
    private val id : Int = chapitreNum

    override fun doInBackground(vararg urls: URL?): JSONObject? {
        var url = URL("https://prevent-game-api.herokuapp.com/api/chapter/"+this.id);
        var s: String? = null

        try {
            val urlConnection =
                url.openConnection() as HttpURLConnection
            try {
                //getting the stream
                val `in`: InputStream =
                    BufferedInputStream(urlConnection.inputStream)
                s = readStream(`in`)
                println(s)
            } finally {
                urlConnection.disconnect()
                //returning the json contained in the stream (= the page minus 2 parenthesis and the first word)
                return JSONObject(s).getJSONObject("data");
            }
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return null
    }

    protected override fun onProgressUpdate(vararg values: Int?) {

    }

    override fun onPostExecute(JSONchapitre: JSONObject?) {
        try {
            //extracting the array containing the images in the json
            if (JSONchapitre != null) {
                val JSONeventList: JSONArray = JSONchapitre.getJSONArray("events")
                val eventList: MutableList<EventHistoire> = mutableListOf()
                for (i in 0 until JSONeventList.length()) {
                    val event: EventHistoire
                    val JSONevent: JSONObject = JSONeventList.getJSONObject(i)
                    if (JSONevent.has("choix")) {
                        val nextEvents: MutableList<Int> = mutableListOf()
                        val choix: MutableList<String> = mutableListOf()
                        val JSONchoixList: JSONArray = JSONevent.get("choix") as JSONArray
                        for (j in 0 until JSONchoixList.length()) {
                            val JSONchoix: JSONObject = JSONchoixList.get(j) as JSONObject
                            nextEvents.add(JSONchoix.get("nextID") as Int)
                            choix.add(JSONchoix.get("texte") as String)
                        }
                        event = Choix(nextEvents, JSONevent.get("id") as Int, JSONevent.get("question") as String, choix)
                    } else {
                        val listDialog: MutableList<Dialogue> = mutableListOf()
                        val JSONdialogList: JSONArray = JSONevent.get("dialogues") as JSONArray
                        for (j in 0 until JSONdialogList.length()) {
                            val JSONdialog: JSONObject = JSONdialogList.get(j) as JSONObject
                            val JSONperso: JSONObject = JSONdialog.get("personnage") as JSONObject
                            listDialog.add(Dialogue(Personnage(JSONperso.get("name") as String,
                                    this.downloadFilesTaskListener.getResId(JSONperso.get("link") as String) as Int, JSONperso.get("isNpc") as Boolean
                            ), JSONdialog.get("text") as String))
                        }
                        event = Conversation(listOf(JSONevent.get("nextId") as Int), JSONevent.get("id") as Int, listDialog)
                    }
                    eventList.add(event)
                }
                var chapitre = Chapitre(JSONchapitre.get("theme") as String,JSONchapitre.get("_id") as Int,JSONchapitre.get("description") as String,JSONchapitre.get("firstEventId") as Int,eventList)
                this.downloadFilesTaskListener.OnDownloadedEvent(chapitre);
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun readStream(`is`: InputStream): String? {
        val sb = StringBuilder()
        val r =
            BufferedReader(InputStreamReader(`is`), 1000)
        var line = r.readLine()
        while (line != null) {
            sb.append(line)
            line = r.readLine()
        }
        `is`.close()
        return sb.toString()
    }

}
