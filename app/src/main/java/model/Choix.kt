package model

import java.util.*


class Choix(next: List<Int>,id: Int,_question: String,_choices: List<String> ): EventHistoire(next,id) {
    var question: String = _question
    var choices: List<String> = _choices
}
