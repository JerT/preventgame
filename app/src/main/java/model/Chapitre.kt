package model

class Chapitre(var _theme: String, var _id: Int, var _description: String, var _firstEvent : Int,var _events :List<EventHistoire>) {
    private  var theme :String = _theme
    private var id: Int = _id
    private var description :String = _description
    private var firstEvent : Int = _firstEvent
    var events :List<EventHistoire> = _events
}

