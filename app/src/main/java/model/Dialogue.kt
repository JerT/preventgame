package model

class Dialogue(_personnage: Personnage? = null,_text: String ) {
    var text : String = _text
    var personnage : Personnage? = _personnage
}

class Personnage(_name: String, _linkImage: Int, _isNpc: Boolean = true) {
    var isNpc : Boolean = _isNpc
    var name : String = _name
    var linkImage : Int = _linkImage

    override fun toString(): String {
        return name
    }
}